/**
Author: Prasanna Venkadesh
License: GNU GPL v3.0

Version: 2.0.0
**/

#include <DHT.h>
#include <ArduinoJson.h>
#include <ArduinoJson.hpp>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

#define LEDonBoard 2  // NodeMCU (ESP8266) has On board LED connected on PIN 2.
#define DHTPIN 4 // the data pin of DHT11 Sensor connected to the GPIO PIN 4 (D2 in nodeMCU)
#define DHTTYPE DHT11 // type of the sensor

HTTPClient https;
std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
DHT dht(DHTPIN, DHTTYPE);

// WiFi Credentials
const char* ssid = "";           // name of the WiFi network to connect
const char* password = "";  // password of the WiFi network

/**
  Platforms to Publish. Disabled by default.
  Set to true if you want to publish to those platforms.
**/
const bool openSenseMap = false;
const bool windy = false;

// OpenSenseMap API Info. Create a new box in OpenSenseMap and obtain these values
const char* api_auth = "";  // OpenSenseMap Access Token
const char* box_id = "";  // OpenSenseMap Box ID
const char* temp_id = ""; // OpenSenseMap Sensor ID for Temperature
const char* hum_id = "";  // OpenSenseMap Sensor ID for Humidity
const String osm_api_url = "https://api.opensensemap.org/boxes/" + String(box_id) + "/data";

// Windy API Info. Register a station in https://stations.windy.com
const String w_api_key = "";
const String w_api_url = "https://stations.windy.com/pws/update/" + w_api_key;

bool firstMeasurement = true;
const long measureInterval = 900000;  // 15 minutes
long blinkInterval = 1000;

unsigned long lastBlinkedMillis = 0;  // last time the LED blinked
unsigned long lastMeasuredMillis = 0; // last time a measurement was taken

StaticJsonDocument<16> doc;
String json;


bool publishToOpenSenseMap(float temperature, float humidity) {
  bool published = false;

  doc[temp_id] = temperature;
  doc[hum_id] = humidity;
  serializeJson(doc, json);

  Serial.println("Publishing vaules to OpenSenseMap...");

  https.begin(*client, osm_api_url);
  https.addHeader("Content-Type", "application/json");
  https.addHeader("Authorization", api_auth);

  int httpStatusCode = https.POST(json);
  Serial.print("HTTP Status Code: ");
  Serial.println(httpStatusCode);

  if (httpStatusCode == 201)
    published = true;

  https.end();

  return published;
}


bool publishToWindy(float temperature, float humidity) {
  bool published = false;

  // append measurement values as query params
  String w_url = w_api_url + "?temp=" + String(temperature) + "&humidity=" + String(humidity);

  Serial.println("Publishing measurements to Windy...");

  https.begin(*client, w_url);
  int httpStatusCode = https.GET();
  Serial.print("HTTP Status Code: ");
  Serial.println(httpStatusCode);

  if (httpStatusCode == 200)
    published = true;

  https.end();

  return published;
}


void setup() {
  // setup code here, to run once:
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  dht.begin();

  pinMode(LEDonBoard,OUTPUT); // OnBoard LED
  digitalWrite(LEDonBoard, HIGH); //--> Turn off OnBoard LED

  // Connect to WiFi Access Point.
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    digitalWrite(LEDonBoard, LOW);
    delay(250);
    digitalWrite(LEDonBoard, HIGH);
    delay(250);
    
  }
  //----------------------------------------
  digitalWrite(LEDonBoard, HIGH); //--> Turn off the On Board LED when it is connected to the wifi router.
  //----------------------------------------If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Successfully connected to : ");
  Serial.println(ssid);
}


void loop() {
  unsigned long currentMillis = millis();
  
  /**
  This condition blinks LEDonBoard once in 15s for 50ms.
  **/
  if (currentMillis - lastBlinkedMillis >= blinkInterval) {
    int ledState = digitalRead(LEDonBoard);
    if (ledState == LOW) {
      digitalWrite(LEDonBoard, HIGH);
      blinkInterval = 15000;
    } else {
      digitalWrite(LEDonBoard, LOW);
      blinkInterval = 50;
    }
    lastBlinkedMillis = currentMillis;
  }

  /**
  This block measures sensor values once in 15 minutes and
  publishes them to enabled platforms.
  **/
  if ((currentMillis - lastMeasuredMillis >= measureInterval) or firstMeasurement) {
    if (WiFi.status() == WL_CONNECTED) {
      client->setInsecure();

      // perform measurement
      float temp = dht.readTemperature();
      float humid = dht.readHumidity();

      // check if values are measured
      if (isnan(temp) or isnan(humid)) {
        Serial.println("Failed to read values from Sensor.");
      } 
      else {
        Serial.println("Temperature: " + String(temp) + " Humidity: " + String(humid));

        if (openSenseMap) {
          // publish measurements to OpenSenseMap
          bool publishedToOSM = publishToOpenSenseMap(temp, humid);
          if (publishedToOSM)
            Serial.println("Published measurements to OpenSenseMap.");
          else
            Serial.println("Publish to OpenSenseMap failed.");
        }

        if (windy) {
          // publish readings to Windy
          bool publishedToWindy = publishToWindy(temp, humid);
          if (publishedToWindy)
            Serial.println("Published measurements to Windy.");
          else
            Serial.println("Publish to Windy failed.");
        }
      }
    }

    lastMeasuredMillis = currentMillis;
    firstMeasurement = false;
  }
}
