## NodeMCU (ESP8266) Weather Station

[NodeMCU](https://www.nodemcu.com/index_en.html) based Weather Station with [DHT11 Sensor](https://learn.adafruit.com/dht/overview) to measure Temperature & Humidity and publish the data to [OpenSenseMap](https://opensensemap.org).

### Parts Required:
1. [NodeMCU (ESP8266) board.](https://makerbazar.in/products/node-mcu-wemos-d1-wifi-internet-module)
2. [DHT11 - Temperature & Humidity Sensor](https://makerbazar.in/products/humidity-sensor-module-dht-11)
3. [Jumper Wires](https://makerbazar.in/products/jumper-cable-male-female) - A minimum of 4 Female-Female Jumper wires should be sufficient, but since it's cheap, having 10 in each category would help in the future use cases.
4. USB A to Micro USB Cable (same microUSB cable used for charging phones)

**Note**: Provided e-commerce links to parts are for demonstration only. It need not be bought only from there, it can be bought from any store.

### Firmware

NodeMCU boards comes with a firmware with the same name NodeMCU which allows us to program it in a langugage called **lua**. The board's firmware can be flashed with either [Micropython firmware](https://micropython.org/download/ESP8266_GENERIC/) to code in **Python** or **Arduino** firmware to code in **cpp**.

*As of writing this, Micropython v1.22.2 has a TLS limitation, where it doesn't support TLS1.3 for ESP8266 boards. Because of this, data cannot be published to OpenSenseMap or any website that uses TLS1.3, which most of them will. Hence proceeding with Arduino firmware.*

### Tools Required:
1. A PC with [Arduino IDE](https://www.arduino.cc/en/software)

### Connection, Wiring & Power Supply

![](images/circuit-breadboard.jpg)

1. Some DHT11 sensors have 4 pins and some have 3 pins. In either case only 3 pins are used.
2. Connect the Ground (GND/G) pin of DHT11 sensor to one of the G pin in NodeMCU.
3. Connect the VCC or + pin of DHT11 sensor to one of the 3V pin in NodeMCU.
4. Connect the Data pin of DHT11 sensor to D2 (GPIO 4) pin in NodeMCU.
5. Finally connect the board and PC with the USB A to MicroUSB cable to power the device.

### Libraries Required

The code depends on the following libraries which can be downloaded using Arduino IDE's library manager.
1. DHT Sensor Library by Adafruit (v1.4.6)
2. Adafruit Unified Sensor by Adafruit (v1.1.4)
3. ArduinoJSON by Benoit Blanchon (v7.0.4)

### OpenSenseMap Account Registration

Note: If you do not wish to publish (or) upload the measurements to OpenSenseMap, then skip it.

1. If you don't have an account in [OpenSenseMap](https://opensensemap.org/), go ahead and sign-up for a new account and Register a new Sensebox.
2. While registering choose **Manual Configuration** in the **Hardware** section and add two sensors as following.
3. Add Sensor -> Phenomenon -> Temperature, Unit -> C, Type -> DHT11
4. Add Sensor -> Phenomenon -> Humidity, Unit -> %, Type -> DHT11
5. Once registered, API access tokens, Box ID and Sensor IDs will be generated and shown.

### Windy Account Registration

Note: If you do not wish to publish (or) upload the measurements to Windy, then skip it.

1. Create a account in [Windy](https://stations.windy.com/), go ahead and sign-up for a new account and register a new station.
2. **Type of the station** -> "other station type"; **sharing of your data** -> "open"; **Specify type** -> "Custom Built - NodeMCU".
3. **Elevation (m)** -> visit [Topography Map](https://en-gb.topographic-map.com/), search your city and get elevation value.
4. **Tempmeter AGL** -> denotes the height of your station from the ground floor. Please figure out and enter the value here.
5. Once registered, **API Key** can be found by clicking **Show key** button in the station page.

### Flashing Sketch to Board

1. Once board is connected to PC, open Arduino IDE.
2. Download [esp8266_weather_station.ino](esp8266_weather_station.ino) file and open it in Arduino IDE.
3. If you wish to publish data to `OpenSenseMap` or `Windy`, change boolean value from `false` to `true`.
4. Fill the values for the following variables (`ssid, password`, (`api_auth, box_id, temp_id, hum_id` - OpenSenseMap) and `w_api_key` - Windy) in the code.
5. Goto File -> Preferences and find `Additional boards manager URLS` and paste the URL `https://arduino.esp8266.com/stable/package_esp8266com_index.json`. This adds supports for NodeMCU ESP8266 boards for Arduino. Wait for sometime for the packages to be indexed.
6. Goto **Tools** -> **Manage Libraries**, search and install the libraires from the above **Libraries Required** section.
7. Goto **Tools** -> **Board** -> **esp8266** -> **NodeMCU 1.0 (ESP-12E Module)**.
8. Goto **Tools** -> **Port** -> and select the port shown (this is the USB port NodeMCU is connected to PC)
9. If the above steps went well, Goto **Sketch** -> **Upload** (or press the Upload button below the Menu bar).

### Board Operation

1. Once the board is flashed, LEDs will blink while connecting to WiFi network.
2. To understand what's going on in the Microcontroller, in the Arduino IDE, goto Tools -> Serial Monitor and observe the logs.
3. As soon as the board is connected to WiFi, it will start the first measurement and publish to enabled platforms and wait for 15 minutes before next measurement.
4. To make sure everything is going good, goto OpenSenseMap or Windy website and check if the measurements are shown there.
5. If everything looks good, find a independent power source for your board (Powerbank or good old phone charges 5V-1A output.) and find an ideal place for measuring indoor (or) outdoor temperature and place it there.

Collecting data and publishing them to an Open database (public domain) like OpenSenseMap is the first step. Welcome to Citizen Science!

### Improvements

1. Feel free to experiment with better sensors. [DHT22](https://makerbazar.in/products/dht22-am2302-temperature-and-humidity-sensor-module-for-arduino-rpi-stm-dht22-with-pcb) and [BMP280](https://makerbazar.in/products/bmp280-barometric-pressure-and-altitude-sensor-i2c-spi-module) are better than DHT11 in terms of measured values. There are other sensors like BMP/BME series as well. 
2. Add more sensors (pressure, anemometer, rain guage, etc.) to make it a full fledged weather station.
3. Explore best practices on where and how to place the sensors outdoor / indoor for proper measurements.
