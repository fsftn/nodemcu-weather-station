## [Unreleased]

### Added

- Device Configuration through WiFi (WiFi Manager, choose platforms to upload data)

### Changed

- Secure TLS connection to platforms.

## [2.0.0]

### Added

- Support to publish measured values to Windy.com
- Option to choose between platforms to upload (or) disable them together.

## [1.0.0]

### Added

- DHT11 Sensor support. Measures Temperature & Relative Humidity.
- Connect to WLAN, provided credentials are configured.
- Publish measured values to OpenSenseMap.org
- Heartbeat LED blink every 15 seconds once.
